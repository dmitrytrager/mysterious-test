require "rails_helper"

describe PostSerializer do
  let(:post) { build :post, id: 1, user_id: 1, title: 'title', body: 'body' }
  let(:json) { ActiveModel::SerializableResource.serialize(post).to_json }
  let(:post_json) { parse_json(json)["post"] }

  it "returns post" do
    expect(post_json).to be_a_post_representation(post)
  end
end
