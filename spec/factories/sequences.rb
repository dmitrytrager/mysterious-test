FactoryGirl.define do
  sequence(:email) { |n| "user#{n}@example.com" }
  sequence(:password) { "123456" }

  sequence(:title) { |n| "Post #{n} title" }
  sequence(:body) { Faker::Lorem.paragraphs }
  sequence(:text) { Faker::Lorem.paragraph }
end
