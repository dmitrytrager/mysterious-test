FactoryGirl.define do
  factory :user do
    email
    password

    trait :admin do
      admin true
    end
  end
end
