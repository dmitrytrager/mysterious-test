require "rails_helper"
require "rspec_api_documentation/dsl"

resource "Posts" do
  let!(:post) { create :post }
  let!(:posts) { [post] }
  let!(:id) { post.id }

  header "Accept", "application/json"

  subject(:response) { json_response_body }

  get "/v1/posts" do
    example_request "Listing posts" do
      expect(response["posts"]).to be_a_kind_of Array
      expect(response["posts"].first).to be_a_post_representation(post)
    end
  end

  post "/v1/posts" do
    parameter :title, "Post title", required: true
    parameter :body, "Post body", required: true

    let(:post_params) { attributes_for(:post).slice(:title, :body) }
    let(:params) { { post: post_params } }

    it_behaves_like(
      "a method that requires an authentication",
      "post",
      "creating"
    )

    context "with authentication token" do
      let(:author) { create :user }

      before { set_authentication_header(author) }

      example_request "Creating post" do
        expect(response_status).to eq(201)
        expect(response["post"]).to be_a_post_representation(Post.last)
      end
    end
  end

  get "/v1/posts/:id" do
    example_request "Retrieving post" do
      expect(response["post"]).to be_a_post_representation(post)
    end
  end

  put "/v1/posts/:id" do
    parameter :title, "Post title"
    parameter :body, "Post body"

    let(:post_params) do
      post.attributes.slice(:title, :body).merge(title: "New post")
    end
    let(:params) { { post: post_params } }

    it_behaves_like(
      "a method that requires an authentication",
      "post",
      "updating"
    )

    context "with authentication token" do
      before do
        set_authentication_header(author)
        post.assign_attributes post_params
      end

      context "when user is not a post author" do
        let(:author) { create :user }

        example_request "Updating post" do
          expect(response_status).to eq(401)
          expect(response["error"]).to include("not allowed")
        end

        context "when user is an admin" do
          let(:author) { create :user, :admin }

          example_request "Updating post" do
            expect(response["post"]).to be_a_post_representation(post)
          end
        end
      end

      context "when user is a post author" do
        let(:author) { post.user }

        example_request "Updating post" do
          expect(response["post"]).to be_a_post_representation(post)
        end
      end
    end
  end

  delete "/v1/posts/:id" do
    it_behaves_like(
      "a method that requires an authentication",
      "post",
      "deleting"
    )

    context "with authentication token" do
      before do
        set_authentication_header(author)
      end

      context "when user is not a post author" do
        let(:author) { create :user }

        example_request "Deleting post" do
          expect(response_status).to eq(401)
          expect(response["error"]).to include("not allowed")
        end

        context "when user is an admin" do
          let(:one_more_post) { create :post }
          let(:id) { one_more_post.id }
          let(:author) { create :user, :admin }

          example_request "Deleting post" do
            expect(response_status).to eq(200)
            expect(response["post"]).to be_a_post_representation(one_more_post)
          end
        end
      end

      context "when user is a post author" do
        let(:author) { post.user }

        example_request "Deleting post" do
          expect(response_status).to eq(200)
          expect(response["post"]).to be_a_post_representation(post)
        end
      end
    end
  end
end
