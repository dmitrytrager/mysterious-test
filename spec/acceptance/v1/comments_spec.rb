require "rails_helper"
require "rspec_api_documentation/dsl"

resource "Comments" do
  let!(:post) { create :post }
  let!(:comment) { create :comment, post: post }
  let!(:comments) { [comment] }
  let!(:id) { comment.id }
  let!(:post_id) { post.id }

  header "Accept", "application/json"

  subject(:response) { json_response_body }

  get "/v1/posts/:post_id/comments" do
    example_request "Listing comments" do
      expect(response["comments"]).to be_a_kind_of Array
      expect(response["comments"].first).to be_a_comment_representation(comment)
    end
  end

  post "/v1/posts/:post_id/comments" do
    parameter :text, "Comment text", required: true

    let(:comment_params) { attributes_for(:comment).slice(:text) }
    let(:params) { { comment: comment_params } }

    it_behaves_like(
      "a method that requires an authentication",
      "comment",
      "creating"
    )

    context "with authentication token" do
      let(:author) { create :user }

      before { set_authentication_header(author) }

      example_request "Creating comment" do
        expect(response_status).to eq(201)
        expect(response["comment"]).to be_a_comment_representation(Comment.last)
      end
    end
  end

  get "/v1/posts/:post_id/comments/:id" do
    example_request "Retrieving comment" do
      expect(response["comment"]).to be_a_comment_representation(comment)
    end
  end

  put "/v1/posts/:post_id/comments/:id" do
    parameter :text, "Comment text"

    let(:comment_params) do
      post.attributes.slice(:text).merge(text: "New comment")
    end
    let(:params) { { comment: comment_params } }

    it_behaves_like(
      "a method that requires an authentication",
      "comment",
      "updating"
    )

    context "with authentication token" do
      before do
        set_authentication_header(author)
        comment.assign_attributes comment_params
      end

      context "when user is not a comment author" do
        let(:author) { create :user }

        example_request "Updating comment" do
          expect(response_status).to eq(401)
          expect(response["error"]).to include("not allowed")
        end

        context "when user is an admin" do
          let(:author) { create :user, :admin }

          example_request "Updating comment" do
            expect(response["comment"]).to be_a_comment_representation(comment)
          end
        end
      end

      context "when user is a comment author" do
        let(:author) { comment.user }

        example_request "Updating comment" do
          expect(response["comment"]).to be_a_comment_representation(comment)
        end
      end
    end
  end

  delete "/v1/posts/:post_id/comments/:id" do
    it_behaves_like(
      "a method that requires an authentication",
      "comment",
      "deleting"
    )

    context "with authentication token" do
      before do
        set_authentication_header(author)
      end

      context "when user is not a comment author" do
        let(:author) { create :user }

        example_request "Deleting comment" do
          expect(response_status).to eq(401)
          expect(response["error"]).to include("not allowed")
        end

        context "when user is an admin" do
          let(:one_more_comment) { create :comment, post: post }
          let(:id) { one_more_comment.id }
          let(:author) { create :user, :admin }

          example_request "Deleting comment" do
            expect(response_status).to eq(200)
            expect(response["comment"]).to be_a_comment_representation(one_more_comment)
          end
        end
      end

      context "when user is a comment author" do
        let(:author) { comment.user }

        example_request "Deleting comment" do
          expect(response_status).to eq(200)
          expect(response["comment"]).to be_a_comment_representation(comment)
        end
      end
    end
  end
end
