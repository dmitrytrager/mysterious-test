# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = FactoryGirl.create :user, email: "author@example.com"
commenter = FactoryGirl.create :user, email: "commenter@example.com"

post = FactoryGirl.create :post, user: user, title: "Post", body: "Story"
FactoryGirl.create :comment, user: commenter, post: post, text: "Comment"
