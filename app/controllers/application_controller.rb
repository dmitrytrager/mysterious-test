class ApplicationController < ActionController::API
  include ActionController::ImplicitRender
  include ActionController::HttpAuthentication::Token::ControllerMethods

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!, only: [:create, :update, :destroy]

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  respond_to :json

  def authenticate_user!
    authenticate_user_from_token!
    super
  end

  def authenticate_user_from_token!
    authenticate_or_request_with_http_token do |user_token, _|
      user = fetch_user_by_token(user_token)

      if user
        sign_in user, store: false
      end
    end
  end

  private

  def fetch_user_by_token(token)
    token && User.where(authentication_token: token).first
  end

  def user_not_authorized(error)
    render json: { error: error.to_s }, status: :unauthorized
  end
end
