module V1
  class PostsController < ApplicationController
    expose(:posts)
    expose(:post, attributes: :post_params)

    def index
      respond_with posts
    end

    def create
      post.user = current_user
      authorize post
      post.save

      respond_with post
    end

    def show
      respond_with post
    end

    def update
      authorize post
      post.save

      respond_with post
    end

    def destroy
      authorize post
      post.destroy

      respond_with post
    end

    private

    def post_params
      params.require(:post).permit(
        :title,
        :body
      )
    end
  end
end
