module V1
  class CommentsController < ApplicationController
    expose(:post)
    expose(:comments, ancestor: :post)
    expose(:comment, attributes: :comment_params)

    def index
      respond_with comments
    end

    def create
      comment.user = current_user
      authorize comment
      comment.save

      respond_with comment
    end

    def show
      respond_with comment
    end

    def update
      authorize comment
      comment.save

      respond_with comment
    end

    def destroy
      authorize comment
      comment.destroy

      respond_with comment
    end

    private

    def comment_params
      params.require(:comment).permit(
        :post_id,
        :text
      )
    end
  end
end
