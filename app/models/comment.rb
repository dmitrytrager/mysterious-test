class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user

  validates :user, :post, :text, presence: true
end
