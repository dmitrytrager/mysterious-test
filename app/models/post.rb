class Post < ActiveRecord::Base
  belongs_to :user

  has_many :comments

  validates :user, :title, presence: true
end
