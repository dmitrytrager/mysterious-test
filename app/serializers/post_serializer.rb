class PostSerializer < ApplicationSerializer
  attributes :id, :user_id, :title, :body
end
