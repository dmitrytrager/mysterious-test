class CommentSerializer < ApplicationSerializer
  attributes :id, :user_id, :post_id, :text
end
